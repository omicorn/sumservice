Sum Service
===========

Authors
-------
Vilho Kivihalme (506024), Jarko Papalitsas (501250)

Description
-----------
This is a university project for distributed sum service (University of Turku 2013)

Usage
-----
Start by executing SumService in bin folder

Summauspalvelu
==============

Tekijät
-------
Vilho Kivihalme (506024), Jarko Papalitsas (501250)

Kuvaus
------
Harjoitustyö hajautetun summauspalvelimen kehittämisestä (Turun Yliopisto 2013)

Käyttö
------
Käynnistetään suorittamalla SumService bin-kansiosta
