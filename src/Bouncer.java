import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Logger;

/**
 * Ylläpitää pääkomentoja vastaanottavaa TCP-palvelinta ja välittää komennot eteenpäin
 * @author Vilho Kivihalme (506024), Jarko Papalitsas (501250)
 * 
 */
public class Bouncer implements Runnable {
	private static final Logger LOGGER = Logger.getLogger(Bouncer.class.getName());
	Lobby lobby;
	Socket messageSocket;
	ObjectInputStream inputStream;
	ObjectOutputStream outputStream;
	
	/**
	 * 
	 * @param l Yhteiset datat sisältävä olio
	 */
	public Bouncer(Lobby l) {
		this.lobby = l;
	}
	
	/**
	 * Alustaa palvelimen yhteydet
	 * @param timeout Palvelimen alustuksen aikakatkaisuraja
	 * @throws IOException
	 */
	private void initialize(int timeout) throws IOException {
		this.messageSocket = this.lobby.getListenerSocket().accept();
		lobby.setTcpReady(true);
		this.messageSocket.setSoTimeout(timeout);
		LOGGER.info("Connection established");
		this.inputStream = new ObjectInputStream(messageSocket.getInputStream());
		this.outputStream = new ObjectOutputStream(messageSocket.getOutputStream());
		int serverCount = this.inputStream.readInt();
		LOGGER.info("Requested " + serverCount + " adder servers");
		for (int i=1; i<=serverCount; i++) {
			outputStream.writeInt(this.lobby.createSumServer());
		}
			outputStream.flush();


	}
	
	/**
	 * Sulkee viestivirrat sekä pistokeyhteydet
	 * @throws IOException
	 */
	private void terminate() throws IOException {
		outputStream.close();
		inputStream.close();
		messageSocket.close();
	}

	/**
	 * Ylläpitää palvelinyhteyden ja ajaa komennot
	 */
	@Override
	public void run() {
		try {
			int timeout = 5000;
			initialize(timeout);
			this.messageSocket.setSoTimeout(lobby.getTIMEOUT());
			int code;
			while ((code = inputStream.readInt())!=0) {
				outputStream.writeInt(parseCommands(code));
				outputStream.flush();
			}
			this.terminate();
		} catch (SocketTimeoutException s) {
			try {
				this.outputStream.writeInt(-1);
				this.outputStream.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			LOGGER.warning("Socket inactive for too long, terminating");
		} catch (IOException e) {
			LOGGER.warning("Socket stream was interrupted, terminating");
		}
		
	}
		
	/**
	 * Parsii komennon ja suorittaa vastaavan funktion
	 * 
	 * @param command Komentokoodi, jonka palvelin lähettää
	 */
	private int parseCommands(int command) {
		switch (command) {
		case 1: return lobby.getTotalSum();
		
		case 2: return lobby.getHighestCount();
	
		case 3: return lobby.getTotalCount();
		
		default: return -1;
		}
		
	}

}
