import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import javax.naming.TimeLimitExceededException;

/**
 * Luokka toimii rajapintana jaettuun tietoon.
 * @author Vilho Kivihalme (506024), Jarko Papalitsas (501250)
 * 
 */

public class Lobby {
	private static final Logger LOGGER = Logger.getLogger(Lobby.class.getName());
	private boolean tcpReady = false;
	private ServerSocket listenerSocket;
	private final int[] PORTPOOL = {5550,5551,5552,5553,5554,5555,5556,5557,5558,5559};
	private final int TIMEOUT = 60000;
	private boolean[] portInUse;
	private List<int[]> sumContainers;
	
	/**
	 * 
	 * @param listeningPort Kuunteluportti pääkomennoille
	 * @throws IOException
	 */
	public Lobby(int listeningPort) throws IOException {
		this.listenerSocket = new ServerSocket(listeningPort);
		portInUse = new boolean[PORTPOOL.length];
		sumContainers = Collections.synchronizedList(new ArrayList<int[]>());
	}
	
	/**
	 * 
	 * @return Servereiden yhteisen epäaktiivisuuden aikakatkaisuraja
	 */
	public int getTIMEOUT() {
		return TIMEOUT;
	}

	/**
	 * 
	 * @return Pääkomentoja vastaanottavan TCP-yhteyden kuuntelupistoke
	 */
	public ServerSocket getListenerSocket() {
		return listenerSocket;
	}

	/**
	 * 
	 * @return Onko TCP-yhteys muodostettu palvelimen ja asiakkaan välille
	 */
	public boolean isTcpReady() {
		return tcpReady;
	}

	/**
	 * 
	 * @param tcpReady TCP-yhteyden valmiuden tila
	 */
	public void setTcpReady(boolean tcpReady) {
		this.tcpReady = tcpReady;
	}
	
	/**
	 * Ilmoittaa palvelimelle UDP-paketilla palvelun saatavuustiedot
	 * 
	 * @param serverAddress Palvelimen IP-osoite
	 * @param announcePort Palvelimen ilmoitusportti, johon paketti lähetetään
	 * @param retryCount Uudelleenyritysten määrä, mikäli TCP-yhteyttä ei saada luotua aikarajaan mennessä
	 * @param retryInterval Uudelleenyritysten väliaika millisekunteina
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws TimeLimitExceededException 
	 */
	public void announce(InetAddress serverAddress, int announcePort, int retryCount, int retryInterval) throws IOException, InterruptedException, TimeLimitExceededException {
		String helloMessage = Integer.toString(this.listenerSocket.getLocalPort());
		LOGGER.info("Listening on port " + helloMessage);
		DatagramSocket announceSocket = new DatagramSocket();
		DatagramPacket tcpPort = new DatagramPacket(helloMessage.getBytes(), helloMessage.getBytes().length, serverAddress, announcePort);
		for (int i = 1; i <= retryCount; i++) {
			if (!tcpReady) {
				announceSocket.send(tcpPort);
				LOGGER.info("Announce attempt " +(i + " of " + retryCount));
				Thread.sleep(retryInterval);
				if (!tcpReady && i == retryCount) {
					LOGGER.warning("Connection timed out");
					announceSocket.close();
					throw new TimeLimitExceededException();
				}
			}
			else{
				break;
			}
				
		}
		announceSocket.close();
	}
	
	/**
	 * 
	 * @return Porttinumero luotuun ja käynnistettyyn summauspalvelimeen
	 */
	public int createSumServer() {
		for(int i = 0;i<PORTPOOL.length;i++){
			if(!portInUse[i]){
				portInUse[i] = true;
				sumContainers.add(new int[2]);
				SumServer s = new SumServer(PORTPOOL[i],sumContainers.get(i), this.getTIMEOUT());
				Thread sumThread = new Thread(s);
				sumThread.start(); //voisi laittaa tarkistuksen, että TCP-palvelin kuuntelee, mutta käytännössä kuitenkin ehtii aina
				return PORTPOOL[i];
			}
		}

		return -1;
	}
		
	/**
	 * 
	 * @return Tähänmennessä välitettyjen lukujen yhteissumma
	 */
	public int getTotalSum() {
		int totalSum = 0;
		for (int[] i:sumContainers) {
			totalSum = i[0] + totalSum;
		}
		LOGGER.info("Total sum requested, sending " + totalSum);
		return totalSum;
	}

	
	/**
	 * 
	 * @return Tähän mennessä välitettyjen lukujen kokonaismäärä
	 */
	public int getTotalCount() {
		int totalCount = 0;
		for (int[] i:sumContainers) {
			totalCount = i[1] + totalCount;
		}
		LOGGER.info("Total count requested, sending " + totalCount);
		return totalCount;
	}
	
	/**
	 * 
	 * @return Suurimman arvon omaavan Summauspalvelimen indeksinumero
	 */
	public int getHighestCount(){
		int index = 1;
		int value = sumContainers.get(0)[index-1];
		for (int i = 0;i<sumContainers.size();i++) {
			int[] t = sumContainers.get(i);
			if(value<t[0]){
				value = t[0];
				index = i+1;
			}
		}
		LOGGER.info("Highest count requested, sending " + index);
		return index;
	}
	
}
