import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Summauspalvelin laskee sille TCP:n yli annetut luvut yhteen ja
 * lisää sille annettuun lokeroon Lobbyssä.
 * @author Vilho Kivihalme (506024), Jarko Papalitsas (501250)
 *
 */
public class SumServer implements Runnable{
	private static final Logger LOGGER = Logger.getLogger(SumServer.class.getName());
	private final int PORT;
	private final int TIMEOUT;
	private int[] sums;
	ServerSocket listenerSocket;
	Socket numberSocket;
	ObjectInputStream inputStream;
	
	/**
	 * 
	 * @param port Portti, jota summauspalvelin kuuntelee
	 * @param sums Lokero, johon summat ja lukumäärä talletetaan
	 * @param timeout Epäaktiivisuuden aikakatkaisuraja, jonka jälkeen palvelin sammuttaa itsensä viimeistään
	 */
	public SumServer(int port, int[] sums, int timeout) {
		this.PORT = port;
		this.sums  = sums;
		this.TIMEOUT = timeout;

	}
	
	/**
	 * Alustaa palvelimen yhteydet
	 * @param timeout Palvelimen alustuksen aikakatkaisuraja
	 * @throws IOException
	 */
	private void initialize(int timeout) throws IOException {
		LOGGER.info("Initializing " + this);
		this.listenerSocket = new ServerSocket(this.PORT);
		this.listenerSocket.setSoTimeout(timeout);
		this.numberSocket = listenerSocket.accept();
		this.numberSocket.setSoTimeout(timeout);
		this.inputStream = new ObjectInputStream(numberSocket.getInputStream());

	}
	
	/**
	 * Sulkee viestivirrat sekä pistokeyhteydet
	 * @throws IOException
	 */
	private void terminate() throws IOException {
		LOGGER.info("Closing " + this);
		this.inputStream.close();
		this.numberSocket.close();
		this.listenerSocket.close();
	}
	
	public String toString() {
		return "Server at port " + this.PORT;
	}

	/**
	 * Lisää arvon aikaisempiin arvoihin
	 * @param number Lisättävä arvo
	 */
	private void calculateSum(int number) {
		LOGGER.fine(this + "is calculating number " + number);
		sums[0] = sums[0] + number;
		sums[1]++;

	}
	
	/**
	 * Ylläpitää palvelinyhteyden ja ajaa komennot
	 */
	@Override
	public void run() {
		try {
			this.initialize(5000);
			this.numberSocket.setSoTimeout(this.TIMEOUT);
			int number;
			while (( number = inputStream.readInt())!=0) {
				calculateSum(number);
			}
			this.terminate();
		} catch (IOException e) {
			LOGGER.warning(this + " was force closed");
		}
		
	}
	
	
	
	
}
