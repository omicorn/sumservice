import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.Logger;
import javax.naming.TimeLimitExceededException;

/**
 * Käynnistysohjelma
 * @author Vilho Kivihalme (506024), Jarko Papalitsas (501250)
 *
 */
public class SumService {
	private static final Logger LOGGER = Logger.getLogger(SumService.class.getName());
	
	public static void main(String[] args) {
		Lobby lobby;
		LOGGER.info("Started Bouncer thread");
		try {
			lobby = new Lobby(8892);
			LOGGER.info("Lobby created");
			new Thread(new Bouncer(lobby)).start();
			try {
			lobby.announce(InetAddress.getLocalHost(), 3126, 5, 5000);
			} catch (TimeLimitExceededException e) {
				lobby.getListenerSocket().close();
			}
		} catch (IOException e) {
			LOGGER.warning("Announce failed: " + e);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
